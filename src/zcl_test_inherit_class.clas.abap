CLASS zcl_test_inherit_class DEFINITION
  PUBLIC
  CREATE PUBLIC .

  PUBLIC SECTION.
  PROTECTED SECTION.

    METHODS example_01.
    METHODS example_02.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_test_inherit_class IMPLEMENTATION.


  METHOD example_01.

    " Code for ZCL_ROOT_CLASS=>EXAMPLE_01( ).
    cl_demo_output=>display(  ).
    cl_demo_output=>display(  ).
    cl_demo_output=>display(  ).
    cl_demo_output=>display( |Test| ).
    cl_demo_output=>display( |Test| ).
    cl_demo_output=>display( |Test| ).

  ENDMETHOD.


  METHOD example_02.

    " Code for ZCL_TEST_INHERIT_CLASS=>EXAMPLE_02( ).

  ENDMETHOD.
ENDCLASS.
